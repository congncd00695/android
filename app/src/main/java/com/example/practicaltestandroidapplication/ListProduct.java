package com.example.practicaltestandroidapplication;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.practicaltestandroidapplication.data.DbHelper;

import javax.xml.namespace.QName;

public class ListProduct extends AppCompatActivity {
    private DbHelper db;
    private Cursor c;
    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_user);

        db = new DbHelper(this);
        ListView lvUser = (ListView) findViewById(R.id.lvUser);

        c = db.getAllProduct();

        adapter = new SimpleCursorAdapter(this, R.layout.item_user, c, new String[]{
                DbHelper.ID, DbHelper.NAME, DbHelper.QUANTITY
        }, new int[]{
                R.id.tvId, R.id.tvName, R.id.tvQuantity
        }, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        lvUser.setAdapter(adapter);

        lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Test View", "Test");
                Cursor cursor = (Cursor) adapter.getItem(position);
                int _id = cursor.getInt(cursor.getColumnIndex(DbHelper.ID));
                String name = cursor.getString(cursor.getColumnIndex(DbHelper.NAME));
                String quantity = cursor.getString(cursor.getColumnIndex(DbHelper.QUANTITY));

                Intent intent = new Intent(String.valueOf(ListProduct.this));
                intent.putExtra(DbHelper.ID, _id);
                intent.putExtra(DbHelper.NAME,name);
                intent.putExtra(DbHelper.QUANTITY, quantity);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        //Reload Data in ListView
        c = db.getAllProduct();
        adapter.changeCursor(c);
        adapter.notifyDataSetChanged();
        db.close();
    }
}
